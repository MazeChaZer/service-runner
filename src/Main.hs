#!/usr/bin/env runhaskell
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
import Control.Concurrent
import Control.Monad
import Data.Aeson
import qualified Data.ByteString.Lazy as BSL
import GHC.Generics
import GHC.IO.Handle
import System.Directory
import System.Environment
import System.Exit
import System.IO
import System.Process
import Data.List (intercalate)
import System.Random (randomRIO)
import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap)

main :: IO ()
main = do
    baseColorIndex <- randomRIO (0, length palette - 1)
    [configFilePath] <- getArgs
    Just config <- decode <$> BSL.readFile configFilePath
    handleServices baseColorIndex config

data ServiceConfig = ServiceConfig
    { name :: String
    , program :: String
    , arguments :: Maybe [String]
    , workingDirectory :: Maybe String
    , environment :: Maybe (HashMap String String)
    } deriving (Generic, FromJSON)

data Service = Service
    { serviceConfig :: ServiceConfig
    , color :: (Int, Int, Int)
    }

handleServices :: Int -> [ServiceConfig] -> IO ()
handleServices baseColorIndex serviceConfigs = do
    let services = map (\(index, config) -> Service config $ selectColor baseColorIndex (length serviceConfigs) index) . zip [0..] $ serviceConfigs
    writerLock <- newMVar ()
    let maxNameLength = maximum . map (length . name . serviceConfig) $ services
    processHandles <-
        mapM (handleService writerLock maxNameLength) services
    mapM_ waitForProcess processHandles

handleService :: MVar () -> Int -> Service -> IO ProcessHandle
handleService lock maxNameLength service = do
    let config = serviceConfig service
    programEnvironment <- HashMap.fromList <$> getEnvironment
    (_, Just subprocessStdout, Just subprocessStderr, processHandle) <-
        createProcess (proc (program config) (maybe [] id $ arguments config))
            { std_out = CreatePipe
            , std_err = CreatePipe
            , cwd = workingDirectory config
            , env = Just . HashMap.toList . (`HashMap.union` programEnvironment) . maybe HashMap.empty id . environment $ config
            , delegate_ctlc = True
            }
    forkIO $ handleStream lock maxNameLength (name config) (color service) subprocessStdout stdout
    forkIO $ handleStream lock maxNameLength (name config) (color service) subprocessStderr stderr
    pure processHandle

handleStream :: MVar () -> Int -> String -> (Int, Int, Int) -> Handle -> Handle -> IO ()
handleStream lock maxNameLength name color inStream outStream = do
    let loop = do
        eof <- hIsEOF inStream
        if eof then do
            pure ()
        else do
            line <- hGetLine inStream
            takeMVar lock
            hPutStrLn outStream $ linePrefix maxNameLength name color ++ line
            hFlush outStream
            putMVar lock ()
            loop
    loop

linePrefix :: Int -> String -> (Int, Int, Int) -> String
linePrefix maxNameLength name (r, g, b) =
    "\x1b[1m\x1b[38;2;" ++ color ++ "m" ++ name ++ dots ++ ">\x1b[0m "
    where
        dots = replicate (maxNameLength - length name) '.'
        color = intercalate ";" . map show $ [r, g, b]

selectColor :: Int -> Int -> Int -> (Int, Int, Int)
selectColor baseColorIndex serviceCount index =
    palette !! ((baseColorIndex + shift) `mod` length palette)
    where shift = round $ fromIntegral (length palette) * fromIntegral index / fromIntegral serviceCount

{-| Generated on http://hclwizard.org:64230/hclwizard/
HUE 1    = 0
HUE 2    = 300
CHROMA 1 = 100
CHROMA 2 = 100
LUMIN. 1 = 60
LUMIN. 2 = 60
POWER 1  = 0
POWER 2  = 0
NUMBER   = 40
-}
palette :: [(Int, Int, Int)]
palette =
    [ (240,94,132)
    , (237,99,114)
    , (232,105,94)
    , (227,111,70)
    , (221,116,37)
    , (214,122,0)
    , (206,127,0)
    , (198,133,0)
    , (188,138,0)
    , (178,142,0)
    , (167,147,0)
    , (154,151,0)
    , (139,155,0)
    , (123,159,0)
    , (103,162,0)
    , (78,165,0)
    , (35,168,0)
    , (0,171,0)
    , (0,173,51)
    , (0,175,78)
    , (0,177,99)
    , (0,178,118)
    , (0,179,135)
    , (0,179,150)
    , (0,179,165)
    , (0,178,178)
    , (0,177,191)
    , (0,175,203)
    , (0,172,214)
    , (0,168,224)
    , (0,163,233)
    , (0,158,240)
    , (0,151,246)
    , (71,144,250)
    , (114,136,252)
    , (143,127,252)
    , (166,119,250)
    , (186,110,246)
    , (201,101,240)
    , (214,94,233)
    ]
