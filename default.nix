{ pkgs ? import <nixpkgs> {} }:
pkgs.stdenv.mkDerivation {
    name = "service-runner";
    src = ./src;
    buildInputs = with pkgs; [
        (haskellPackages.ghcWithPackages
            (haskellPackages: with haskellPackages; [
                aeson unordered-containers
            ])
        )
    ];
    buildPhase = ''
        ghc --make -threaded Main.hs
    '';
    installPhase = ''
        mkdir -p "$out/bin"
        cp Main "$out/bin/service-runner"
    '';
}
