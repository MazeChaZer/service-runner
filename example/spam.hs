#!/usr/bin/env runhaskell
import Control.Concurrent (threadDelay)
import System.Random (randomRIO)
import System.Environment (lookupEnv)

main :: IO ()
main = maybe "spam!" id <$> lookupEnv "MESSAGE" >>= spam

spam :: String -> IO ()
spam message = do
    nap <- randomRIO (0, 1000000)
    threadDelay nap
    putStrLn message
    spam message
